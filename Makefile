MKPATH := $(dir $(lastword $(MAKEFILE_LIST)))
DEFAULT_XORENGINE := $(abspath $(MKPATH)/xorengine/include)
XORENGINE ?= $(DEFAULT_XORENGINE)
MKPATH := $(abspath $(MKPATH))

.phony: drat, pbp, all, clean, noproof

all: pbp

$(DEFAULT_XORENGINE):
	git submodule init
	git submodule update

drat: Makefile $(XORENGINE)
	make -C ./ clean
	MROOT=$(MKPATH) \
	XORENGINE=$(XORENGINE) \
	CFLAGS="-DUSE_DRAT_PROOF -DNDEBUG" \
	LFLAGS="" \
	make -C ./simp
	cp ./simp/minisat ./bin/minisat_drat

pbp: Makefile $(XORENGINE)
	make -C ./ clean
	MROOT=$(MKPATH) \
	XORENGINE=$(XORENGINE) \
	CFLAGS="-DUSE_PBP_PROOF -DNDEBUG" \
	LFLAGS="" \
	make -C ./simp
	cp ./simp/minisat ./bin/minisat_pbp

noproof: Makefile $(XORENGINE)
	make -C ./ clean
	MROOT=$(MKPATH) \
	XORENGINE=$(XORENGINE) \
	CFLAGS="-DNDEBUG" \
	LFLAGS="" \
	make -C ./simp
	cp ./simp/minisat ./bin/minisat_noproof

clean:
	find . -name minisat -or -name "*.o" -delete
